This is a private repo to support integration of [microG](https://microg.org/) with RattlesnakeOS. The author of RattlesnakeOS doesn't recommend the use of microG. It is currently the required signature spoofing patch and just a subset of prebuilt packages from here: https://github.com/FriendlyNeighborhoodShane/MinMicroG_releases

## How to
Add the following to end of your `rattlesnakeos-stack` config file.
```
[[custom-patches]]
  patches = ["00005-enable-signature-spoofing.patch"]
  repo = "https://gitlab.com/vsoze/aosp-patches"

[[custom-prebuilts]]
  modules = [
    "GmsCore",
    "GsfProxy",
    "FakeStore",
    "com.google.android.maps.jar",
    "AppleNLPBackend",
	"DejaVuNLPBackend",
    "LocalGSMNLPBackend",
    "LocalWiFiNLPBackend",
    "MozillaUnifiedNLPBackend",
    "NominatimNLPBackend",
    "AuroraDroid",
    "AuroraServices",
    "AuroraStore",
    "default-permissions",
    "sysconfig"
  ]
  repo = "https://gitlab.com/vsoze/microg"
```

The first time you boot, open the microG app and do the self-test. Give it the permissions and add location backends. If it still complains that network location is not enabled, you need to toggle the location from main android settings once for it to work. Ideally, you should get all check boxes after that.

## Credits
* Adapted from https://github.com/RattlesnakeOS/microg
